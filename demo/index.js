import React from 'react'
import {render} from 'react-dom'
import {Grid, Column, Row} from '../src'
import styles from './style.css'
import vars, {names} from '../src/vars'

const Card = props => (
  <Column {...props} className={styles.card}/>
)

const Debug = props => <Column {...props} className={styles.debug}/>

const columns = vars['column-count']

const App = () => {
  const full = `var(${columns})`
  const half = `calc(var(${columns}) / 2)`
  const oneFourth = `calc(var(${columns}) / 4)`
  const threeFourth = `calc(var(${columns}) / 4 * 3)`

  return (
    <Grid margin={6} padding={10}>
      <Card s={full} m={half} l={oneFourth}>
        <h1>The "Minus minus grid"</h1>
        <h2>powered by css variables</h2>
        <p>"minus minus grid" uses the power of css variables a.k.a. custom properties. The results is a lightweight grid with a few lines of boilerplate and full flexibility.</p>
      </Card>
      <Card s={full} m={half} l={threeFourth}>
        <h3>Grid, Row, Column</h3>
        <p>This grid uses three different CSS selectors.</p>
        <ul>
          <li><code>.grid</code></li>
          <li><code>.row</code></li>
          <li><code>.column</code></li>
        </ul>
        <h3>Endless nesting, same logic</h3>
        <p>A grid can be nested endlessly while preserving the column size</p>
        <Row>
          <Debug s={full}>
            <Row>
              <Debug s={full}>
                <Row>
                  <Debug s={half} l={4}/>
                  <Debug s={half} l={5}/>
                  <Debug s={full}>
                    <Row>
                      <Debug s={full}/>
                      <Debug s={2}/>
                      <Debug s={2}/>
                      <Debug s={2}/>
                      <Debug/>
                      <Debug l={2}/>
                      <Debug s={3} m={4} l={3}/>
                      <Debug s={full} l={6}/>
                    </Row>
                  </Debug>
                </Row>
              </Debug>
            </Row>
          </Debug>
        </Row>
      </Card>
      <Card s={full} m={half}>
        <h3>Advanced config</h3>
        <blockquote>Does not work in Safari!</blockquote>
        <pre>
/* always 1/2 of its parent */{'\n'}
{names[0]}: calc(var({columns}) / 2);{'\n'}
{'\n'}
/* always 3/4 of its parent */{'\n'}
{names[0]}: calc(var({columns}) / 4 * 3);{'\n'}
{'\n'}
/* change per breakpoint */{'\n'}
{names[0]}: calc(var({columns}) / 4 * 3);{'\n'}
{names[1]}: calc(var({columns}) / 2);{'\n'}
{names[2]}: calc(var({columns}) / 3);{'\n'}
        </pre>
      </Card>
      <Card s={full} m={half}>
        <h3 >Browser support </h3>
        <ul>
          <li>😍 Chrome </li>
          <li>😍 Chromium</li>
          <li>😍 Firefox</li>
          <li>🙂 Safari -{' '}
            <a href='https://bugs.webkit.org/show_bug.cgi?id=161002' target='_blank'>bug</a>
          </li>
          <li>❓ Edge -{' '}
            <a href='https://wpdev.uservoice.com/forums/257854-microsoft-edge-developer/suggestions/6261292-css-variables'
               target='_blank'>status</a>
           </li>
          <li>💩 Internet Explorer </li>
        </ul>
        <h5>Support</h5>
        <ul>
          <li>😍 full </li>
          <li>🙂 partial</li>
          <li>❓ unknown </li>
          <li>💩 none </li>
        </ul>
        <h5>Known issues: (2016-10-25)</h5>
        <ul>
          <li>🐢 Microsoft is working on support for CSS variables.</li>
          <li>🐞 Safari does not fully support</li><code>calc()</code> with custom properties.
        </ul>
      </Card>
      <Debug s={4} m={5} l={8}/>
      <Debug s={4} m={4}/>
      <Debug s={4} m={8}/>
      <Debug s={full}>
        <Row>
          <Debug s={half}/>
          <Debug s={half}/>
          <Debug s={full}>
            <Row>
              <Debug s={half}/>
              <Debug s={half}/>
              <Debug s={full}>
                <Row>
                  <Debug s={half} m={full} l={half}/>
                  <Debug s={half}/>
                  <Debug s={full} m={half} l={full}>
                    <Row>
                      <Debug s={half} m={oneFourth} l={half}/>
                      <Debug s={half} m={threeFourth} l={half}/>
                    </Row>
                  </Debug>
                </Row>
              </Debug>
            </Row>
          </Debug>
        </Row>
      </Debug>
      <Debug s={half} m={oneFourth}/>
      <Debug s={half} m={threeFourth}/>
      <Debug s={half}>
        <Row>
          <Debug s={half}/>
          <Debug s={half}/>
        </Row>
      </Debug>
    </Grid>
  )
}

const mountPoint = document.getElementById('mountPoint')
render(<App/>, mountPoint)
