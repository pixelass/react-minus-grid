import React, {PropTypes} from 'react'
import classNames from 'classnames'
import styles from '../minus-grid.css'

/**
 * creates a div that assigns css variables inline
 * @param  {Object} props
 * @param  {PropTypes.node} props.children content of the row
 * @param  {String} props.className additional classNames
 * @return {PropTypes.node} returns a react element with the row logic
 * @example
 * <Grid>
 *   <Column m={4} l={8}/>
 *   <Column m={4}>
 *     <Row>
 *       <Column m={4}/>
 *       <Column m={4}/>
 *       <Column m={2}/>
 *       <Column m={2}/>
 *     </Row>
 *   </Column>
 * </Grid>
 */
const Row = props => <div className={classNames(styles.row, props.className)}>{props.children}</div>

Row.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
}

export default Row
