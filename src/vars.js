const ABCQ = require('abcq')

const shortId = new ABCQ({
  counter: 123456789
})

/**
 * creates a var with a hash
 * @param  {String} prefix prefix before hash
 * @return {String} returns a css variable with a random hash
 */
const hashVar = (prefix = 'var') => {
  return `--${prefix}-${shortId.generate()}`
}

module.exports = {
  'names': [
    hashVar('vp-small'),
    hashVar('vp-medium'),
    hashVar('vp-large')
  ],
  'margin': hashVar('margin'),
  'padding': hashVar('padding'),
  'column-count': hashVar('columns')
}
