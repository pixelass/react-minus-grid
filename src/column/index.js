import React, {Component, PropTypes} from 'react'
import classNames from 'classnames'
import styles from '../minus-grid.css'
import {names} from '../vars'

class Column extends Component {
  /**
   * creates a div that assigns css variables inline
   * @param  {Object} props
   * @param  {Number | String} props.s the --viewport-small colspan
   * @param  {Number | String} props.m the --viewport-medium colspan
   * @param  {Number | String} props.l the --viewport-large colspan
   * @param  {PropTypes.node} props.children content of the column
   * @param  {String} props.className additional classNames
   * @return {ReactElement} returns a react element with the column logic
   * @example
   * <Column/>
   * <Column className='custom'/>
   * <Column s={4}/>
   * <Column s={2}
   *         m={full}/>
   */
  constructor (props) { // eslint-disable-line
    super(props)
  }

  componentDidMount () {
    this.setProps()
  }

  componentDidUpdate () {
    this.setProps()
  }

  setProps () {
    const {s, m, l} = this.props
    this.root.style.setProperty(names[0], s)
    this.root.style.setProperty(names[1], m)
    this.root.style.setProperty(names[2], l)
  }

  render () {
    return (
      <div className={classNames(styles.column, this.props.className)}
           ref={x => { this.root = x }}>
        {this.props.children}
      </div>
    )
  }
}

Column.propTypes = {
  s: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  m: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  l: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  className: PropTypes.string,
  children: PropTypes.node
}

export default Column
