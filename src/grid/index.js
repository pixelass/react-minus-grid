import React, {Component, PropTypes} from 'react'
import classNames from 'classnames'
import styles from '../minus-grid.css'
import {margin as m, padding as p} from '../vars'

class Grid extends Component {
  /**
   * creates a div that assigns css variables inline
   * @param  {Object} props
   * @param  {Number} props.margin the global grid margin
   * @param  {Number} props.padding the global grid padding
   * @param  {PropTypes.node} props.children content of the grid
   * @param  {String} props.className additional classNames
   * @return {PropTypes.node} returns a react element with the grid logic
   * @example
   * <Grid/>
   * <Grid className='custom'/>
   * <Grid margin={4}/>
   * <Grid margin={10}
   *       padding={10}/>
   */
  constructor (props) { // eslint-disable-line
    super(props)
  }

  componentDidMount () {
    this.setProps()
  }

  componentDidUpdate () {
    this.setProps()
  }

  setProps () {
    const {margin, padding} = this.props
    this.root.style.setProperty(p, margin)
    this.root.style.setProperty(m, padding)
  }

  render () {
    return (

        <div className={classNames(styles.grid, this.props.className)}
             ref={x => { this.root = x }}>
          {this.props.children}
        </div>
    )
  }
}

Grid.propTypes = {
  margin: PropTypes.number,
  padding: PropTypes.number,
  className: PropTypes.string,
  children: PropTypes.node
}

export default Grid
