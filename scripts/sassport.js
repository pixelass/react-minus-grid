const sassport = require('sassport')
const fs = require('fs')

const path = partial => `${__dirname}/../${partial}`

sassport().render({
  file: path('src/index.scss')
}, (err, result) => {
  if (err) {
    throw err
  }
  const {css} = result
  fs.writeFile(path('src/minus-grid.css'), css.toString())
})
